var $ = jQuery = require('jquery');
var Handlebars = require('handlebars');
var _ = require('lodash');

$(function(){

    $.getJSON('data/users.json', function(usersData){

        $.getJSON('data/comments.json', function (comments) {

            var prevData = null;

            _.forEach(comments, function(value, key) {
               var user = _.find(usersData, {'username': value.username });
               value.fullName = user.fullName;

               var d = new Date(value.time);
               var day = (d.getDate() < 10 ?  "0" + d.getDate(): d.getDate());
               var hours = (d.getHours() < 10 ? '0' + d.getHours() : d.getHours());
               var hoursFormatted = (d.getHours() < 10 ? '0' + d.getHours() : d.getHours());
               var minutes = (d.getMinutes() < 10 ? '0' + d.getMinutes() : d.getMinutes());
               var seconds = (d.getSeconds() < 10 ? '0' + d.getSeconds() : d.getSeconds());
               var dayFormat = hours >= 12 ? 'PM' : 'AM';
               var year = d.getFullYear();

               var month = (d.getMonth == 0 ? 'Jan':
                       d.getMonth == 1 ? 'Fed':
                       d.getMonth == 2 ? 'March':
                       d.getMonth == 3 ? 'Apr':
                       d.getMonth == 4 ? 'May':
                       d.getMonth == 5 ? 'Jun':
                       d.getMonth == 6 ? 'July':
                       d.getMonth == 7 ? 'Aug':
                       d.getMonth == 8 ? 'Sept':
                       d.getMonth == 9 ? 'Oct':
                       d.getMonth == 10 ? 'Nov':
                       'Dec'
               );

               value.timeFormatted = hoursFormatted + ':' + minutes + ':' + seconds  + ':' + dayFormat;

               var data =  month + ' ' + day + ' ' + year;

                   if (!prevData || prevData !== data) {
                       prevData = data;
                       value.date = data;
                   }
            });

            var commentsList = {};
            commentsList.comments = comments;

            var template = $('#comments').html();
            var compiledTemplate = Handlebars.compile(template);

            $('.comment-group').append(compiledTemplate(commentsList));
            $('.comments-header').html(commentsList.comments.length + ' comment' + (commentsList.comments.length <=1 ? '' : 's'));
        });
    });
});
