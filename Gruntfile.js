'use strict';

module.exports = function(grunt) {
    //Load all grunt tasks
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-browserify');

    grunt.initConfig({
        watch: {
            js: {
                files: ['public/js/app.js'],
                tasks: ['browserify']
            },
            css: {
                files: "public/styles/less/*.less",
                tasks: ["less"]
            }
        },
        browserify: {
            js: {
                src: 'public/js/*.js',
                dest: 'public/build/bundle.js'
            },
            css: {
                files: "public/styles/less/*.less",
                tasks: ["less"]
            }
        },
        less: {
            development: {
                files: {
                    "public/build/css/app.css": "public/styles/less/app.less"
                }
            }
        }
    });

    grunt.registerTask('default', ['watch']);
};
